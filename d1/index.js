//console.log("Hello World");
//[SECTION] Document Object Model
//It allows us to be able to access or modify the properties of an element in a webpage.
//it is a standard on how to get, change, add or delete HTML elements.
/*
	Syntax: document.querySelector("htmlElement");
	-The querySelector function takes a string input that is formatted like a CSS Selector when applying styles.
*/

//Alternative way on retrieving HTML elements

// document.getElementById("txt-first-name");
// document.getElementByClassName("txt-last-name");
// However, using this functionn requires us to identify beforehand we get the element. with querySelector, we can be flexible in how to retrieve elements.

//para magamit sa susunod kaya contain muna cxa
const txtFistName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
const chosenColor =  document.querySelector("#colors");

//[SECTION] Event Listeners
//whenever a user interacts with a web page, this action is considered as an event.(example: mouse click, mouse hover, page load, key press, etc.)

// addEventListener takes two arguements:
	//a string that identify an event.
	//a function that the listener will execute once the "specified event" is triggered.
							//(event, function)
/*							
txtFistName.addEventListener("keyup", () =>{
	//"innerHTML" property sets or retirn the HTML content( inner HTML) of an element (div,span, etc.).
	// ".value" property sets or returns the value of an attribute (from contols).
	spanFullName.innerHTML = `${txtFistName.value}`;
});
*/
//When the evet occurs, an "event object" is passed to the function argument as the forst parameter.
/*
txtFistName.addEventListener("keyup", (event) => {
	//The "event.target" contains the element where the event happened.
	console.log(event.target);
	//The "event.target.value" gets the value of the input object(txt-first-name)
	console.log(event.target.value);
})
*/
//Creating Multiple event that use the same function.
const fullName = () =>{
	spanFullName.innerHTML = `${txtFistName.value} ${txtLastName.value}`; 
}

txtFistName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);


//Activity

/*
Create another addEventListener that will "change the color" of the "spanFullName". Add a "select tag" element with the options/values of red, green, and blue in your index.html.

Check the following links to solve this activity:
	HTML DOM Events: https://www.w3schools.com/jsref/dom_obj_event.asp
	HTML DOM Style Object: https://www.w3schools.com/jsref/dom_obj_style.asp

*/

chosenColor.addEventListener("change", ()=> {
	spanFullName.style.color = chosenColor.value;
})
